package com.epam.edu.online.model;

import java.util.ArrayList;
import java.util.List;

public class PizzaFactory {
    private static List<Pizza> pizzas = new ArrayList<>();
    static {
        pizzas.add(new Pizza("Margherita", 68));
        pizzas.add(new Pizza("Marinara", 55));
        pizzas.add(new Pizza("Quattro ", 110));
        pizzas.add(new Pizza("Carbonara", 62));
        pizzas.add(new Pizza("Frutti ", 60));
        pizzas.add(new Pizza("Quattro ", 80));
        pizzas.add(new Pizza("Crudo", 64));
        pizzas.add(new Pizza("Napoletana", 66));
        pizzas.add(new Pizza("Pugliese", 81));
        pizzas.add(new Pizza("Montanara", 76));
    }
    public static List<Pizza> getPizza() {
        return pizzas;
    }
}
