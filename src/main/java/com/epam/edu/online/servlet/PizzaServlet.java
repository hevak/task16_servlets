package com.epam.edu.online.servlet;

import com.epam.edu.online.model.Pizza;
import com.epam.edu.online.model.PizzaFactory;
import com.epam.edu.online.pages.HtmlPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/orderPizza", name = "pizzaServlet", displayName = "pizza servlet")
public class PizzaServlet extends HttpServlet {
    private static final Logger log = LogManager.getLogger(PizzaServlet.class);
    static Map<Pizza, Integer> bucket = new LinkedHashMap();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HtmlPage.getHeader(resp);
        resp.getWriter().println("    <h2>Order pizza</h2>");
        resp.getWriter().println("<a href='main'>main page</a>");
        resp.getWriter().println("    <h4>Choose pizza</h4>");
        resp.getWriter().println("    <div id='bucket'>");
        resp.getWriter().println("    <h4>Bucket: </h4>");
        resp.getWriter().println("<ul>");
        for (Map.Entry<Pizza, Integer> pizza : bucket.entrySet()) {
            resp.getWriter().println("<li>" + pizza.getKey().getName()
                    + "(" + pizza.getValue() + ") - "
                    + pizza.getKey().getCost() * pizza.getValue() + "UAH. </li>");
        }
        resp.getWriter().println("</ul>");
        resp.getWriter().println("Sum: <b>" + bucket.entrySet().stream()
                .map(pizza -> pizza.getKey().getCost() * pizza.getValue())
                .reduce(Double::sum)
                .orElse(0.0)
                + "</b>");
        resp.getWriter().println("<a href='bucket'>continue</a>");
        resp.getWriter().println("</div>");
        for (Pizza pizza : PizzaFactory.getPizza()) {
            resp.getWriter().println(HtmlPage.getFormPost(pizza));
        }
        HtmlPage.getFooter(resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        PizzaFactory.getPizza().stream()
                .filter(pizza -> pizza.getId() == id)
                .findFirst()
                .ifPresent(pizza -> bucket.compute(pizza, (pizza1, integer) -> integer == null ? 1 : integer + 1));
        doGet(req, resp);
    }

}
