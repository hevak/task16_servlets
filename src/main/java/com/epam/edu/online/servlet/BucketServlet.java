package com.epam.edu.online.servlet;

import com.epam.edu.online.model.Pizza;
import com.epam.edu.online.pages.HtmlPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet(urlPatterns = "/bucket/*", name = "bucketServlet", displayName = "bucket servlet")
public class BucketServlet extends HttpServlet {
    private static final Logger log = LogManager.getLogger(PizzaServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HtmlPage.getHeader(resp);
        resp.getWriter().println("<h2>Bucket pizza</h2>");
        resp.getWriter().println("<a href='orderPizza'>back</a>");
        resp.getWriter().println("<ul>");
        for (Map.Entry<Pizza, Integer> pizza : PizzaServlet.bucket.entrySet()) {
            resp.getWriter().println("<li>" + HtmlPage.getFormDelete(pizza.getKey(), pizza.getValue()) + "</li>");
        }
        resp.getWriter().println("</ul>");
        resp.getWriter().println(
                "<script type=\"application/javascript\">\n" +
                "    function removePizza(id) {\n" +
                "        fetch('/bucket/' + id, {method: 'DELETE'});\n" +
                "    }\n" +
                "</script>");
        HtmlPage.getFooter(resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String[] split = req.getRequestURI().split("/");
        int length = split.length;
        int id = Integer.parseInt(split[length - 1]);
        PizzaServlet.bucket.entrySet().removeIf(pizza -> pizza.getKey().getId() == id && pizza.getValue() == 1);
        PizzaServlet.bucket.entrySet().stream()
                .filter(pizza -> pizza.getKey().getId() == id && pizza.getValue() > 1)
                .limit(1)
                .forEach(pizza -> pizza.setValue(pizza.getValue() - 1));
        doGet(req, resp);
    }
}
