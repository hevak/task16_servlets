package com.epam.edu.online.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/main", name = "mainServlet", displayName = "Main servlet")
public class MainServlet extends HttpServlet {
    private static final Logger log = LogManager.getLogger(MainServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.getWriter().println("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Servlet</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <h1>Main page</h1>\n" +
                "    <a href=\"orderPizza\">Order pizza</a>\n" +
                "</body>\n" +
                "</html>");
    }

}
