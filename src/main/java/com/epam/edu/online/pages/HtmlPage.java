package com.epam.edu.online.pages;

import com.epam.edu.online.model.Pizza;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HtmlPage {
    public static void getHeader(HttpServletResponse resp) throws IOException {
        resp.getWriter().println("<!DOCTYPE html>");
        resp.getWriter().println("<html lang='en'>");
        resp.getWriter().println("<head>");
        resp.getWriter().println("    <meta charset='UTF-8'>");
        resp.getWriter().println(getStyle());
        resp.getWriter().println("    <title>Title</title>");
        resp.getWriter().println("</head>");
        resp.getWriter().println("<body>");
    }

    public static void getFooter(HttpServletResponse resp) throws IOException {
        resp.getWriter().println("</body>");
        resp.getWriter().println("</html>");
    }


    public static String getStyle() {
        return "    <style>\n" +
                "        div#bucket {\n" +
                "        position: absolute;\n" +
                "        right: 20px;\n" +
                "        top: 20px;\n" +
                "        }\n" +
                "    </style>";
    }

    public static String getFormPost(Pizza pizza) {
        return "<form action=\"orderPizza\" method=\"post\">" +
                "    <b>" + pizza.getName() + "</b>" +
                "    <input hidden type=\"number\" name='id' value=\"" + pizza.getId() + "\"/>" +
                "    <input type=\"submit\" value='add to bucket'/>\n" +
                "</form>";
    }
    public static String getFormDelete(Pizza pizza, Integer count) {
        return "<form>" +
                "    <b>" + pizza.getName() + " - " + count + " pizza</b>" +
                "    <input hidden type=\"number\" name='id' value=\"" + pizza.getId() + "\"/>" +
                "    <input type=\"button\" onclick='removePizza(this.form.id.value)' value='remove from bucket'/>\n" +
                "</form>";
    }
}
